import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

const VALIDATORS_DATA = {
  name: [
    Validators.required, 
    Validators.pattern(/^[A-Z][a-z]*((\-|\s)[A-Z][a-z]*)*$/), 
    Validators.maxLength(20)
  ],
  age: [
    Validators.required, 
    Validators.pattern(/\d{1,2}/)
  ],
};

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.familyForm = new FormGroup({
      name: new FormControl(null, VALIDATORS_DATA.name),
      father: this.createMemberFormGroup(),
      mother: this.createMemberFormGroup(),
      children: new FormArray([
        this.createMemberFormGroup(),
      ]),
    });
  }
  private createMemberFormGroup (): FormGroup {
    return new FormGroup({
      name: new FormControl(null, VALIDATORS_DATA.name),
      age: new FormControl(null, VALIDATORS_DATA.age),
    });
  }
  public addChild() {
    const child = this.createMemberFormGroup();
    this.children.push(child);
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    this.familyService.addFamily$(this.familyForm.value).subscribe();
    this.familyForm.reset();
  }
}
